#!/bin/sh
LOGFILE="$HOME/lightdm_setup.log"

echo "" >> "$LOGFILE"
#echo `date +%Y-%m-%d:%H:%M:%S.%N` >> "$LOGFILE"
echo "[$(date +%Y-%m-%d:%H:%M:%S.%N)] $USER - $HOME" >> "$LOGFILE"
if [ -e $HOME/.lightdm_cleanup ]; then
	echo "[$(date +%Y-%m-%d:%H:%M:%S.%N)] User cleanup script file found! Executing..." >> "$LOGFILE"
	su -c "$HOME/.lightdm_cleanup" $USER >> "$LOGFILE" 2>&1;
fi
