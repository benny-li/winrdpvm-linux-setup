```
# Building FreeRDP

git clone https://github.com/FreeRDP/FreeRDP

sudo apt-get install --yes build-essential git-core cmake xsltproc libssl-dev libx11-dev libxext-dev libxinerama-dev libxcursor-dev libxdamage-dev libxv-dev libxkbfile-dev libasound2-dev libcups2-dev libxml2 libxml2-dev libxrandr-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libxi-dev libgstreamer-plugins-base1.0-dev libavutil-dev libavcodec-dev

cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_SSE2=ON .

make

# Install FreeRDP
sudo make install

sudo echo "/usr/local/lib/freerdp" >> /etc/ld.so.conf.d/freerdp.conf
sudo echo "/usr/local/lib" >> /etc/ld.so.conf.d/freerdp.conf

sudo ldconfig

xfreerdp -u <username>  --app --plugin rail --data "%windir%\system32\cmd.exe" -- <ip of your windows machine>
```

```


xfreerdp /network:broadband /u:thin /p:remote /v:192.168.1.120 /app:"%SystemDrive%\Program Files\Microsoft Office\root\Office16\EXCEL.EXE" /app-name:"Excel 2013" /app-icon:~/Documents/Private/ownCloud/scripts/vbox/icons/Excel-2013.png +clipboard +fonts /drive:/home/Documents
```
