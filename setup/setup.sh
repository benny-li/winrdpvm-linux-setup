#!/usr/bin/env bash

#####
#
# TODO
#  * Notify errors
#
#  * Retry to start app on errors 5 times
#    * Reset connection http://hope-this-helps.de/serendipity/archives/219-RDP-Session-verwalten-mittels-qwinsta-und-rwinsta.html
#
#  * Save State on Shutdown / Reboot / Hibernate:
#    http://askubuntu.com/a/417288
#
#####

# Uncomment to do a dry run (only print what should be done)
#DRY_RUN="YES"

source /etc/swim/swim.config

BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../src" && pwd )"
#echo "$BASE_PATH"

function backup_existing () {
  local DEST_PATH="$1"
  if [ -f "$DEST_PATH" ] && [ ! -L "$DEST_PATH" ]; then
    # TODO What if the backup already exists?
    echo "Creating backup of $DEST_PATH"
    [[ ! $DRY_RUN ]] && mv "$DEST_PATH" "$DEST_PATH.old"
    [[ ! $DRY_RUN ]] && chmod -x "$DEST_PATH.old"
  fi
}


function link () {
  # Resolve relative paths with readlink
  local SRC_PATH="$(readlink -f "$1")"
  local DEST_PATH="$2"

  backup_existing "$DEST_PATH"

  if [ -L "$DEST_PATH" ]; then
    echo "Removing old symlink at $DEST_PATH"
    [[ ! $DRY_RUN ]] && rm $DEST_PATH
  fi

  echo "Linking $SRC_PATH to $DEST_PATH"
  [[ ! $DRY_RUN ]] && ln -s "$SRC_PATH" "${DEST_PATH}"
}


function link_recursive () {
  local SRC_PATH="$1"
  local DEST_PATH="$2"
  if [ -d "$SRC_PATH" ]; then
    for link_file in $(ls "$SRC_PATH"); do
      echo "Creating destination directory tree for $DEST_PATH"
      [[ ! $DRY_RUN ]] && mkdir -p "$DEST_PATH"
      link_recursive "$SRC_PATH/$link_file" "$DEST_PATH/$link_file"
    done
  else
    link "$SRC_PATH" "$DEST_PATH"
  fi
}


#####


# check: are we root?
if [ $(id -u) -ne 0 ]
then
	echo ""
	echo "Superuser privileges needed. Please call this script using 'sudo'/as root." 1>&2
	exit 1
fi


# Link executables
echo
echo
echo "## EXECUTABLES"
echo

for executable in $(ls "$BASE_PATH/bin"); do
  link "$BASE_PATH/bin/$executable" "/usr/local/bin/$executable"
  echo "Set execution permission on $executable"
  [[ ! $DRY_RUN ]] && chmod +x "$BASE_PATH/bin/$executable"
done

for executable in $(ls "$BASE_PATH/sbin"); do
  link "$BASE_PATH/sbin/$executable" "/usr/local/sbin/$executable"
  echo "Set execution permission on $executable"
  [[ ! $DRY_RUN ]] && chmod +x "$BASE_PATH/sbin/$executable"
done


# Link configuration files
echo
echo
echo "## CONFIGURATION FILES"
echo

for config in $(ls "$BASE_PATH/etc"); do
  link_recursive "$BASE_PATH/etc/$config" "/etc/$config"
done



# Link init.d files
echo
echo
echo "## init.d RUNLEVEL FILES"
echo

update-rc.d save-vm-state start 05 0 6 .


# Link user home files
echo
echo
echo "## USER HOME FILES"
echo

for user_home_file in $(ls -A "$BASE_PATH/home"); do
  DEST_PATH="$(readlink -f ~/)/$user_home_file"
  link "$BASE_PATH/home/$user_home_file" "$DEST_PATH"
done


# Autostart entries
echo
echo
echo "## AUTOSTART ENTRIES"
echo

for autostart in $(ls -A "$BASE_PATH/Autostart"); do
  DEST_PATH="$(readlink -f ~/)/.config/autostart/$autostart"
  link "$BASE_PATH/Autostart/$autostart" "$DEST_PATH"
done


# Install icons
echo
echo
echo "## APPLICATION ICONS"
echo

for icon in $(ls -A "$BASE_PATH/AppIcons"); do
  xdg-icon-resource install --size 256 --novendor "$BASE_PATH/AppIcons/$icon"
done


# Link program starter
echo
echo
echo "## APPLICATION LAUNCHER ENTRIES"
echo

for shortcut in $(ls -A "$BASE_PATH/ApplicationShortcuts"); do
  DEST_PATH="/usr/local/share/applications/$shortcut"
  link "$BASE_PATH/ApplicationShortcuts/$shortcut" "$DEST_PATH"
done

update-desktop-database
